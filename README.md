## MIG-RollingUpadte-Jenkins


## Objective
This document helps to know how to create pipeline in jenkins to do rolling update of MIG instance.
- [Comman Practise](https://cloud.google.com/compute/docs/instance-groups/updating-migs)

- Better Approach

Using Automated, proactive updates
Setting the MIG's update type to PROACTIVE offers two primary advantages:

- The rollout of an update happens automatically to your specifications, without the need for additional input after the initial request. You can specify the speed of deployment, the level of disruption to your service, and the scope of the update.
- You can automate partial rollouts, which allows for canary testing.

When you start a proactive rolling update, the MIG actively schedules actions to apply the requested updates to instances as necessary. In many cases, this means deleting and recreating instances proactively.

## Prerequisite
- Configure Jenkins on your VM machine

## What is Jenkinsfile

Inside Jenkins CI/CD, a pipeline is defined as a series of events or tasks which are interconnected in a particular order. In simple terms, Jenkins pipeline is a set of modules or plugins which enable the implementation and integration of Continuous Delivery pipelines within Jenkins.

The Jenkins pipeline has an expandable automation system for building basic or complicated ‘template’ distribution pipelines via the Domain-specific language (DSL) used in the pipeline. There are four states of Continuous Delivery in Jenkins pipeline-

- Build
- Deploy
- Test
- Releas


## Configuring Jenkinsfile to create pipeline


- Install required plugins:

  Pipeline

## Jenkinsfile Code

```
pipeline {
 
    agent any
    
    // |Creating Environment Variables for automating and reusability of code|

    
    environment {
        tag = '${BUILD_NUMBER}'                           // This will give build number
        IMAGE_NAME = 'GIVE-NAME-FOR-IMAGE'                // Provide image name which you have created
        GCE_INSTANCE_NAME = 'NAME-OF-INSTNACE'            // Provide Instance name which you have created
        ZONE = 'GIVE-ZONE-NAME'                           // Provide zone name
        PROJECT = 'GIVE-PROJECT-NAME'                     // Provide your project ID
        PROJECT_ID = 'GIVE-PROJECT-NAME'                  // Provide your project ID
        INSTANCE_TEMPLATE_NAME = 'GIVE-NAME-FOR-TEMPLATE' // Provide Instance template name
        MACHINE_TYPE = 'CHOSE-FOR-MACHINE-TYPE'           // Chose Machine Type
        SUBNETWORK = 'GIVE-SUBNET-NAME'                   // Give Subnet name where you want to deploy
        REGION = 'GIVE-REGION-NAME'                       // Provide region name
        SERVICE_ACCOUNT = 'GIVE-SERIVCE-ACCOUNT-NAME'     // Provide your service account
        TAGS = 'http-server,https-server'                 // Give Network tags
        IMAGE_PROJECT_ID = 'GIVE-PROJECT-NAME'            // Provide your project ID
        MIG_NAME = 'PROVIDE-MIG-WHICH-NEEDS-TO-UPDATE'    // Provide MIG name which needs to update with new Instance template
        
    }

    // |Creating different Stages|


    stages {

        // |First Stage|

        stage('Create Image') {
            steps {
                sh "gcloud compute images create ${env.IMAGE_NAME}-${env.tag} \
  --source-disk=${env.GCE_INSTANCE_NAME} \
  --source-disk-zone=${env.ZONE} --project ${env.PROJECT}"
            }
        }


        // |Second Stage|

        stage('Create Instance Template') {
            steps {
                sh "gcloud beta compute --project=${env.PROJECT_ID} instance-templates create ${env.INSTANCE_TEMPLATE_NAME}-${env.tag} \
    --machine-type=${env.MACHINE_TYPE} \
    --subnet=projects/${env.PROJECT_ID}/regions/${env.REGION}/subnetworks/${env.SUBNETWORK} \
    --no-address \
    --maintenance-policy=MIGRATE \
    --service-account=${env.SERVICE_ACCOUNT} \
    --scopes=https://www.googleapis.com/auth/cloud-platform \
    --region=${env.REGION} \
    --tags=${env.TAGS} \
    --image=${env.IMAGE_NAME}-${env.tag} \
    --image-project=${env.IMAGE_PROJECT_ID} \
    --boot-disk-size=30GB \
    --boot-disk-type=pd-balanced \
    --boot-disk-device-name=${env.INSTANCE_TEMPLATE_NAME}-${env.tag} \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --reservation-affinity=any"
            }
        }

        // |Third Stage|

        stage('Update MIG') {
            steps {
                sh "gcloud compute instance-groups managed rolling-action start-update ${env.MIG_NAME} \
    --version=template=${env.INSTANCE_TEMPLATE_NAME}-${env.tag} \
    --type=proactive \
    --zone=${env.ZONE} \
    --project=${env.PROJECT_ID}"
            }
        }
    }
}


```
## Lets understand different stage of pipeline which will do rolling update

- Environment Variables

```
    environment {
        tag = '${BUILD_NUMBER}'                    
        IMAGE_NAME = 'GIVE-NAME-FOR-IMAGE'                //e.g - 'new-image-for-mig-template'
        GCE_INSTANCE_NAME = 'NAME-OF-INSTNACE'            //e.g - 'vm-instance-for-mig'
        ZONE = 'GIVE-ZONE-NAME'                           //e.g - 'asia-east1-b'
        PROJECT = 'GIVE-PROJECT-NAME'                     //e.g - 'my-playground'
        PROJECT_ID = 'GIVE-PROJECT-NAME'                  //e.g - 'my-playground'
        INSTANCE_TEMPLATE_NAME = 'GIVE-NAME-FOR-TEMPLATE' //e.g - 'template-mig-new'
        MACHINE_TYPE = 'CHOSE-FOR-MACHINE-TYPE'           //e.g - 'e2-micro'
        SUBNETWORK = 'GIVE-SUBNET-NAME'                   //e.g - 'new-subnet-1'
        REGION = 'GIVE-REGION-NAME'                       //e.g - 'asia-east1' 
        SERVICE_ACCOUNT = 'GIVE-SERIVCE-ACCOUNT-NAME'     //e.g - 'mig-update@my-playground.iam.gserviceaccount.com'
        TAGS = 'http-server,https-server'                
        IMAGE_PROJECT_ID = 'GIVE-PROJECT-NAME'            //e.g - 'my-playground'
        MIG_NAME = 'PROVIDE-MIG-WHICH-NEEDS-TO-UPDATE'    //e.g - 'mig-server-dev'
    }
    
```

1- Create Image for Instance Template

This stage will automatically create a new image 

```
stage('Create Image') {
            steps {
                sh "gcloud compute images create ${env.IMAGE_NAME}-${env.tag} \
  --source-disk=${env.GCE_INSTANCE_NAME} \
  --source-disk-zone=${env.ZONE} --project ${env.PROJECT}"
            }
        }
```
        
2- Create Instance Template

This will create Instance template for MIG

```
stage('Create Instance Template') {
            steps {
                sh "gcloud beta compute --project=${env.PROJECT_ID} instance-templates create ${env.INSTANCE_TEMPLATE_NAME}-${env.tag} \
    --machine-type=${env.MACHINE_TYPE} \
    --subnet=projects/${env.PROJECT_ID}/regions/${env.REGION}/subnetworks/${env.SUBNETWORK} \
    --no-address \
    --maintenance-policy=MIGRATE \
    --service-account=${env.SERVICE_ACCOUNT} \
    --scopes=https://www.googleapis.com/auth/cloud-platform \
    --region=${env.REGION} \
    --tags=${env.TAGS} \
    --image=${env.IMAGE_NAME}-${env.tag} \
    --image-project=${env.IMAGE_PROJECT_ID} \
    --boot-disk-size=30GB \
    --boot-disk-type=pd-balanced \
    --boot-disk-device-name=${env.INSTANCE_TEMPLATE_NAME}-${env.tag} \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --reservation-affinity=any"
            }
        }

```

3- Update MIG with new Instance Template

This stage with command will automatically do rolling update for MIG

```
stage('Update MIG') {
            steps {
                sh "gcloud compute instance-groups managed rolling-action start-update ${env.MIG_NAME} \
    --version=template=${env.INSTANCE_TEMPLATE_NAME}-${env.tag} \
    --type=proactive \     
    --zone=${env.ZONE} \
    --project=${env.PROJECT_ID}"
            }
        }
```
